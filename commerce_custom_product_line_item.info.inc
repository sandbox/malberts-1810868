<?php

/**
 * @file
 * Provides metadata for the product entity.
 */


/**
 * Implements hook_commerce_product_type_insert().
 */
function commerce_custom_product_line_item_commerce_product_type_insert($type) {
  // Create the line item type field when creating a new line item.
  commerce_custom_product_line_item_configure_product_type($type['type']);
}

/**
 * Implements hook_entity_property_info_alter() on top of the Product module.
 */
function commerce_custom_product_line_item_entity_property_info_alter(&$info) {
  if (!empty($info['commerce_product']['bundles'])) {
    $properties = array();

    foreach ($info['commerce_product']['bundles'] as $bundle => $bundle_info) {
      $bundle_info += array('properties' => array());
      $properties += $bundle_info['properties'];
    }

    if (!empty($properties['commerce_line_item_type'])) {
      $info['commerce_product']['properties']['commerce_line_item_type'] = $properties['commerce_line_item_type'];
    }
  }

  // Add a property that looks at both the Product and the Product Type to
  // determine the line item type.
  $info['commerce_product']['properties']['line_item_type'] = array(
    'label' => t('Line item type'),
    'description' => t('Line item type calculated from this product or its product Type.'),
    'type' => 'token',
    'getter callback' => 'commerce_custom_product_line_item_getter',
    'calculated' => TRUE,
    'entity views field' => TRUE,
  );
  $info['commerce_product']['properties']['line_item_type_source'] = array(
    'label' => t('Line item type source'),
    'description' => t('Whether the setting comes from the product or its product Type.'),
    'type' => 'token',
    'getter callback' => 'commerce_custom_product_line_item_getter',
    'calculated' => TRUE,
    'entity views field' => TRUE,
  );
}
