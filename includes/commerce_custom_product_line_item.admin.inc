<?php

/**
 * @file
 * Menu callbacks and form builders.
 */


/**
 * Menu callback: product type line item settings.
 */
function commerce_custom_product_line_item_product_type_overview_form_wrapper($type) {
  if (is_array($type)) {
    $product_type = $type;
  }
  else {
    $product_type = commerce_product_type_load($type);
  }

  return drupal_get_form('commerce_custom_product_line_item_summary_form', $product_type);
}

/**
 * Menu callback: product type line item settings.
 */
function commerce_custom_product_line_item_product_overview_form_wrapper($product) {
  if (is_int($product)) {
    $product = commerce_product_load($product);
  }
  $product_type = commerce_product_type_load($product->type);

  return drupal_get_form('commerce_custom_product_line_item_summary_form', $product_type, $product);
}

/**
 * Line item settings overview form.
 */
function commerce_custom_product_line_item_summary_form($form, &$form_state, $product_type, $product = NULL) {
  $form['#product_type'] = $product_type;
  $form['#product'] = $product;

  if (empty($product)) {
    $line_item_type = $product_type['line_item_type'];
    $line_item_type_source = 'product_type';

    // Edit link
    $type_arg = strtr($product_type['type'], '_', '-');
    $edit_link = 'admin/commerce/products/types/' . $type_arg . '/fields/commerce_line_item_type';
  }
  else {
    $product_wrapper = entity_metadata_wrapper('commerce_product', $product);
    $line_item_type = $product_wrapper->line_item_type->value();
    $line_item_type_source = $product_wrapper->line_item_type_source->value();

    switch ($line_item_type_source) {
      case 'product_type':
        $line_item_type_from = t('source: product type <em>@product_type_name</em>', array('@product_type_name' => $product_type['name']));
        break;

      case 'product':
        $line_item_type_from = t('source: product line item field');
        break;
    }

    // Edit link
    $edit_link = 'admin/commerce/products/' . $product->product_id . '/edit';
  }

  $form['#line_item_type'] = commerce_line_item_type_load($line_item_type);
  $form['#line_item_type_source'] = $line_item_type_source;

  $form['line_item_type'] = array(
    '#type' => 'item',
    '#title' => t('Line item type'),
    '#markup' => $form['#line_item_type']['name'],
    '#weight' => -50,
  );

  if (!empty($line_item_type_from)) {
    $form['line_item_type']['#markup'] .= ' (' . $line_item_type_from . ')';
  }

  // Create a link to edit the line item type.
  $form['line_item_type']['#markup'] .= '<br />[' . l(t('edit'), $edit_link, array('query' => drupal_get_destination())) . ']';

  return $form;
}
