Overview
========

The default behavior in Commerce is to set the Add to Cart line item type on
the field display settings. This module changes that by allowing the line item
type to be defined by the Product or Product Type. This allows
Product --> Line item type relationships to be set up without needing to create
a new Product Display type. It also allows one Add to Cart form to display
different line item types depending on the selected Product, but this requires
Javascript for the attribute AJAX refresh.


Setting Product Type default
============================

 - Go to admin/commerce/config/types
 - Go to the Manage Fields section of the specific Product Type.
 - Edit the 'commerce_field_line_item_type' field and set the
 'Product Type Add to Cart line item type' value.


Setting Product override
========================

 - Edit Product directly or via an Inline Entity Form on a Product Display.
 - Set 'Add to Cart line item type' field value.
