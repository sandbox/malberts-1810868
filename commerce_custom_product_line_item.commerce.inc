<?php

/**
 * @file
 * Contains Drupal Commerce specific hook implementations.
 */


/**
 * Implements hook_commerce_product_type_info_alter().
 */
function commerce_custom_product_line_item_commerce_product_type_info_alter(&$types) {
  foreach ($types as $name => &$type) {
    $instance = field_info_instance('commerce_product', 'commerce_line_item_type', $name);

    $instance_settings = commerce_custom_product_line_item_field_instance_line_item_settings($instance);
    $instance_type = $instance_settings['type'];

    if ($instance_type != '_default') {
      // The line item type has been set on the instance settings.
      $type['line_item_type'] = $instance_type;
    }
    else {
      // The line item type must either be defined in hook_product_type_info() or
      // we use the default type.
      if (empty($type['line_item_type'])) {
        // Use the default product line item.
        $type['line_item_type'] = 'product';
      }
    }
  }
}
